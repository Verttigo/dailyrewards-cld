package ch.verttigo.craftok.dailyrewards.Commands;

import ch.verttigo.craftok.dailyrewards.Managers.RewardManager;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class RewardCommands implements CommandExecutor {
    public boolean onCommand(final CommandSender sender, final Command cmd, final String label,
                             final String[] args) {
        if (sender instanceof Player) {
            final Player player = (Player) sender;
            RewardManager.tryReward(player);
        }
        return false;
    }
}
