package ch.verttigo.craftok.dailyrewards.Commands;

import ch.verttigo.craftok.dailyrewards.DL;
import ch.verttigo.craftok.dailyrewards.Managers.DBManager;
import ch.verttigo.craftok.dailyrewards.Managers.RewardManager;
import ch.verttigo.craftok.dailyrewards.Utils.Utils;
import de.dytanic.cloudnet.ext.bridge.player.ICloudPlayer;
import net.md_5.bungee.api.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class AdminCommands implements CommandExecutor {
    private final DL plugin;

    public AdminCommands(final DL plugin) {
        this.plugin = plugin;
    }

    public boolean onCommand(final CommandSender sender, final Command cmd, final String label, final String[] args) {
        if (cmd.getName().equalsIgnoreCase("dailyrewards")) {
            if (sender.hasPermission("dr.admin")) {
                if (args.length == 0 || args[0].equalsIgnoreCase("help") || args.length > 2) {
                    sender.sendMessage(ChatColor.BOLD + "DailyRewards Admin Help");
                    sender.sendMessage(ChatColor.YELLOW + "/dr reload" + ChatColor.WHITE + ChatColor.ITALIC + " Reload all DR files.");
                    sender.sendMessage(ChatColor.YELLOW + "/dr add" + ChatColor.WHITE + ChatColor.ITALIC + " Add reward to config.");
                    sender.sendMessage(ChatColor.YELLOW + "/dr remove <id>" + ChatColor.WHITE + ChatColor.ITALIC + " Remove reward from config.");
                    sender.sendMessage(ChatColor.YELLOW + "/dr reset" + ChatColor.WHITE + ChatColor.ITALIC + " Reset your cooldown.");
                    sender.sendMessage(ChatColor.YELLOW + "/dr reset (player)" + ChatColor.WHITE + ChatColor.ITALIC + " Reset a player's cooldown.");
                    return true;
                }
                if (args[0].equalsIgnoreCase("reload")) {
                    this.plugin.settings.reloadMsg();
                    this.plugin.settings.saveMsg();
                    this.plugin.settings.reloadRewards();
                    sender.sendMessage(ChatColor.YELLOW + "DailyRewards is reloading...");
                }
                if (args[0].equalsIgnoreCase("reset")) {
                    if (sender instanceof Player) {
                        if (args.length == 1) {
                            final Player player = (Player) sender;

                            DBManager.updateCooldownUUID(player.getUniqueId(), 0L);

                            sender.sendMessage(ChatColor.GREEN + "You reset your cooldown.");
                        }
                    } else {
                        sender.sendMessage(ChatColor.RED + "Oops, you can't do this in console");
                        sender.sendMessage(ChatColor.RED + "Try '/dr reset (player)' instead");
                    }
                    if (args.length == 2) {
                        if (Utils.getPlayer(args[1]) == null) {
                            sender.sendMessage(ChatColor.RED + "The specified player is offline.");
                            return true;
                        }
                        ICloudPlayer target = Utils.getPlayer(args[1]);
                        DBManager.updateCooldownUUID(target.getUniqueId(), 0L);
                        sender.sendMessage(ChatColor.GREEN + "You reset " + target.getName() + "'s cooldown.");
                    }
                }
                if (args[0].equalsIgnoreCase("remove") && args.length == 2) {
                    RewardManager.removeReward((Player) sender, Integer.parseInt(args[2]));
                }
                if (args[0].equalsIgnoreCase("add")) {
                    Utils.addRewardGUI((Player) sender);
                }
                return true;
            } else {
                Utils.noPermission((Player) sender);
            }
        }
        return true;
    }
}
