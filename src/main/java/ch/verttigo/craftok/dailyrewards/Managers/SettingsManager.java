package ch.verttigo.craftok.dailyrewards.Managers;

import net.md_5.bungee.api.ChatColor;
import org.bukkit.Bukkit;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.plugin.Plugin;
import org.bukkit.plugin.PluginDescriptionFile;

import java.io.File;
import java.io.IOException;

public class SettingsManager {
    static SettingsManager instance;
    static Plugin p;
    static FileConfiguration msg;
    static File mfile;
    static FileConfiguration rewards;
    static File rewardsFile;

    static {
        SettingsManager.instance = new SettingsManager();
    }

    public static SettingsManager getInstance() {
        return SettingsManager.instance;
    }

    public static FileConfiguration getMsg() {
        return SettingsManager.msg;
    }


    public static FileConfiguration getRewards() {
        return SettingsManager.rewards;
    }


    public void setup(final Plugin p) {
        SettingsManager.mfile = new File(p.getDataFolder(), "messages.yml");
        if (!SettingsManager.mfile.exists()) {
            try {
                SettingsManager.mfile.createNewFile();
            } catch (IOException e) {
                Bukkit.getServer().getLogger().severe(ChatColor.RED + "Could not create messages.yml!");
            }
        }
        SettingsManager.msg = YamlConfiguration.loadConfiguration(SettingsManager.mfile);
        SettingsManager.msg.options().copyDefaults(true);
        SettingsManager.msg.addDefault("no-rewards", "§aRewards§f: §fTu n'as pas de récompense disponible.");
        SettingsManager.msg.addDefault("rewards-available", "§aRewards§f: §fTu as une récompense journalière");
        SettingsManager.msg.addDefault("rewards-win", "§aRewards§f: §fTu as gagné : ");
        SettingsManager.msg.addDefault("no-permission", "§aRewards§f: §fTu n'as pas la permission ");
        this.saveMsg();

        SettingsManager.rewardsFile = new File(p.getDataFolder(), "rewards.yml");
        if (!SettingsManager.rewardsFile.exists()) {
            try {
                SettingsManager.rewardsFile.createNewFile();
            } catch (IOException e) {
                Bukkit.getServer().getLogger().severe(ChatColor.RED + "Could not create rewards.yml!");
            }
        }
        SettingsManager.rewards = YamlConfiguration.loadConfiguration(SettingsManager.rewardsFile);
        SettingsManager.rewards.options().copyDefaults(true);
        SettingsManager.rewards.addDefault("RewardsList", 0);
    }

    public void saveMsg() {
        try {
            SettingsManager.msg.save(SettingsManager.mfile);
        } catch (IOException e) {
            Bukkit.getServer().getLogger().severe(ChatColor.RED + "Could not save messages.yml!");
        }
    }

    public void reloadMsg() {
        SettingsManager.msg = YamlConfiguration.loadConfiguration(SettingsManager.mfile);
    }

    public void saveRewards() {
        try {
            SettingsManager.rewards.save(SettingsManager.rewardsFile);
        } catch (IOException e) {
            Bukkit.getServer().getLogger().severe(ChatColor.RED + "Could not save rewards.yml!");
        }
    }

    public void reloadRewards() {
        SettingsManager.rewards = YamlConfiguration.loadConfiguration(SettingsManager.rewardsFile);
    }

    public PluginDescriptionFile getDesc() {
        return SettingsManager.p.getDescription();
    }
}
