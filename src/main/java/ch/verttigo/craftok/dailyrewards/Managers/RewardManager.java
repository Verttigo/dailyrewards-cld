package ch.verttigo.craftok.dailyrewards.Managers;

import ch.verttigo.craftok.dailyrewards.Crates.Crate;
import ch.verttigo.craftok.dailyrewards.DL;
import ch.verttigo.craftok.dailyrewards.Utils.Utils;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.scheduler.BukkitRunnable;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;

public class RewardManager {


    public static void tryReward(Player player) {
        if (player.hasPermission("dr.claim")) {
            if (!DBManager.getAllowRewardUUID(player)) {
                player.sendMessage(SettingsManager.getMsg().getString("no-rewards"));
                player.playSound(player.getLocation(), Sound.ANVIL_LAND, 1f, 1f);
            } else {
                if (getRewardsListSize() == 0) {
                    player.sendMessage("Aucune reward");
                    return;
                }
                DBManager.updateCooldownUUID(player.getUniqueId(), new Date().getTime());
                ArrayList<ItemStack> items = new ArrayList<>();
                for (int i = 0; i < getRewardsListSize(); i++) {
                    HashMap<String, Object> item = getReward(i);
                    for (int pp = 0; pp < Integer.parseInt(item.get("Percentage").toString()); pp++) {
                        ItemStack itemRe = new ItemStack(Material.getMaterial(item.get("Material").toString()));
                        ItemMeta itemReMeta = itemRe.getItemMeta();
                        ArrayList<String> lore = new ArrayList();
                        lore.add(item.get("Description").toString());
                        itemReMeta.setLore(lore);
                        itemReMeta.setDisplayName(item.get("Name").toString());
                        itemRe.setItemMeta(itemReMeta);
                        items.add(itemRe);
                    }
                }
                Crate crate = new Crate(items, "Récompense journalière", DL.plugin);
                new BukkitRunnable() {
                    @Override
                    public void run() {
                        crate.spin(8, player);
                        cancel();
                    }
                }.runTaskLater(DL.plugin, 5);

            }
        } else {
            Utils.noPermission(player);
        }
    }

    public static void giveReward(String reward, Player p) {
        HashMap<String, Object> rewardData = getRewardFromName(reward);
        p.sendMessage(SettingsManager.getMsg().getString("rewards-win") + rewardData.get("Name"));
        Bukkit.getServer().dispatchCommand(Bukkit.getConsoleSender(), rewardData.get("Command").toString());
    }

    public static void addReward(String rewardName, Material mat, String command, int percentage, String desc) {
        int id = SettingsManager.getRewards().getInt("RewardsList");
        SettingsManager.getRewards().set("Rewards." + id + ".name", rewardName);
        SettingsManager.getRewards().set("Rewards." + id + ".mat", mat.toString());
        SettingsManager.getRewards().set("Rewards." + id + ".command", command);
        SettingsManager.getRewards().set("Rewards." + id + ".percentage", String.valueOf(percentage));
        SettingsManager.getRewards().set("Rewards." + id + ".desc", desc);
        SettingsManager.getRewards().set("RewardsList", (id + 1));
        SettingsManager.getInstance().saveRewards();
    }

    public static void removeReward(Player p, int id) {
        if (getReward(id).get("Name") == null) {
            p.sendMessage("Keske tu me baragouine sale fils de pute");
            return;
        }
        SettingsManager.getRewards().set("Rewards." + id, null);
        int size = getRewardsListSize();
        if (size > id) {
            for (int i = 0; i < size; i++) {
                if (SettingsManager.getRewards().getString("Rewards." + i + ".name") == null && SettingsManager.getRewards().getString("Rewards." + (i + 1) + ".name") != null) {
                    HashMap<String, Object> reward = getReward(i + 1);
                    SettingsManager.getRewards().set("Rewards." + i + ".name", reward.get("Name").toString());
                    SettingsManager.getRewards().set("Rewards." + i + ".mat", reward.get("Material").toString());
                    SettingsManager.getRewards().set("Rewards." + i + ".command", reward.get("Command").toString());
                    SettingsManager.getRewards().set("Rewards." + i + ".percentage", reward.get("Percentage").toString());
                    SettingsManager.getRewards().set("Rewards." + i + ".desc", reward.get("Description").toString());
                    SettingsManager.getRewards().set("Rewards." + (i + 1), null);
                    SettingsManager.getInstance().saveRewards();
                }
            }
        }
        SettingsManager.getRewards().set("RewardsList", size - 1);
        SettingsManager.getInstance().saveRewards();
    }

    public static HashMap getReward(int id) {
        HashMap<String, Object> reward = new HashMap<>();
        reward.put("Name", SettingsManager.getRewards().getString("Rewards." + id + ".name"));
        reward.put("Material", SettingsManager.getRewards().getString("Rewards." + id + ".mat"));
        reward.put("Command", SettingsManager.getRewards().getString("Rewards." + id + ".command"));
        reward.put("Percentage", SettingsManager.getRewards().getString("Rewards." + id + ".percentage"));
        reward.put("Description", SettingsManager.getRewards().getString("Rewards." + id + ".desc"));
        return reward;
    }

    public static HashMap getRewardFromName(String name) {
        for (int i = 0; i < getRewardsListSize(); i++) {
            if (getReward(i).get("Name").toString().equalsIgnoreCase(name)) {
                return getReward(i);
            }
        }
        return null;
    }

    public static int getRewardsListSize() {
        return SettingsManager.getRewards().getInt("RewardsList");
    }

}
