package ch.verttigo.craftok.dailyrewards.Managers;

import de.dytanic.cloudnet.driver.CloudNetDriver;
import de.dytanic.cloudnet.ext.bridge.player.ICloudOfflinePlayer;
import de.dytanic.cloudnet.ext.bridge.player.ICloudPlayer;
import de.dytanic.cloudnet.ext.bridge.player.IPlayerManager;
import org.bukkit.entity.Player;

import java.util.Date;
import java.util.UUID;

public class DBManager {

    public static final IPlayerManager playerManager = CloudNetDriver.getInstance().getServicesRegistry().getFirstService(IPlayerManager.class);

    public static boolean playerExistsUUID(final UUID uuid) {
        ICloudOfflinePlayer cloudOfflinePlayer = playerManager.getOfflinePlayer(uuid);
        return cloudOfflinePlayer.getProperties().getString("DailyRewards") != null;
    }

    public static boolean getAllowRewardUUID(final Player p) {
        final long current = System.currentTimeMillis();
        long millis = DBManager.getCooldownUUID(p.getUniqueId());
        long day = 24 * 60 * 60 * 1000;
        return millis <= current - day;
    }


    public static void createPlayer(final Player player) {
        if (!playerExistsUUID(player.getUniqueId())) {
            ICloudPlayer cloudPlayer = playerManager.getOnlinePlayer(player.getUniqueId());
            cloudPlayer.getProperties().append("DailyRewards", new Date().getTime());
            cloudPlayer.getProperties().append("DailyRewardsStreak", new Date().getTime());
            playerManager.updateOnlinePlayer(cloudPlayer);
        }

    }

    public static void updateCooldownUUID(final UUID uuid, final long cooldown) {
        ICloudPlayer cloudPlayer = playerManager.getOnlinePlayer(uuid);
        cloudPlayer.getProperties().remove("DailyRewards");
        playerManager.updateOnlinePlayer(cloudPlayer);
        cloudPlayer.getProperties().append("DailyRewards", cooldown);
        playerManager.updateOnlinePlayer(cloudPlayer);
    }


    public static long getCooldownUUID(final UUID uuid) {
        return playerManager.getOnlinePlayer(uuid).getProperties().getLong("DailyRewards");
    }

}
