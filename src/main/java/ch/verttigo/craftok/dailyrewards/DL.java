package ch.verttigo.craftok.dailyrewards;

import ch.verttigo.craftok.dailyrewards.Commands.AdminCommands;
import ch.verttigo.craftok.dailyrewards.Commands.RewardCommands;
import ch.verttigo.craftok.dailyrewards.Listeners.JointEvent;
import ch.verttigo.craftok.dailyrewards.Managers.SettingsManager;
import org.bukkit.Bukkit;
import org.bukkit.plugin.java.JavaPlugin;

public class DL extends JavaPlugin {
    public static DL plugin;
    public SettingsManager settings;

    public DL() {
        this.settings = SettingsManager.getInstance();
    }

    public void onEnable() {
        this.getCommand("dailyrewards").setExecutor(new AdminCommands(this));
        this.getCommand("reward").setExecutor(new RewardCommands());
        this.settings.setup(this);
        plugin = this;
        Bukkit.getPluginManager().registerEvents(new JointEvent(), this);
    }
}
