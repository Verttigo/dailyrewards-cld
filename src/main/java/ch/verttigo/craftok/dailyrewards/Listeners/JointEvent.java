package ch.verttigo.craftok.dailyrewards.Listeners;

import ch.verttigo.craftok.dailyrewards.Managers.DBManager;
import ch.verttigo.craftok.dailyrewards.Managers.SettingsManager;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;

public class JointEvent implements Listener {

    @EventHandler
    public void onPlayerJoin(final PlayerJoinEvent e) {
        DBManager.createPlayer(e.getPlayer());
        if (DBManager.getAllowRewardUUID(e.getPlayer())) {
            e.getPlayer().sendMessage(SettingsManager.getMsg().getString("rewards-available"));
        }
    }
}
