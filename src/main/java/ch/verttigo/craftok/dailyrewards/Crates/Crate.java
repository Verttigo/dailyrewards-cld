package ch.verttigo.craftok.dailyrewards.Crates;

import ch.verttigo.craftok.dailyrewards.Managers.RewardManager;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.plugin.Plugin;
import org.bukkit.scheduler.BukkitRunnable;

import java.util.ArrayList;
import java.util.Random;
import java.util.concurrent.ThreadLocalRandom;

public class Crate {

    Random rn = new Random();
    private final Inventory inv;
    private final Plugin plugin;
    private final ArrayList<ItemStack> contents;
    private final InventoryClickHandler helper;

    private int itemIndex = 8;

    public Crate(ArrayList<ItemStack> contents, String name,
                 Plugin main) {
        inv = Bukkit.createInventory(null, 27, name);
        this.plugin = main;
        this.contents = contents;
        helper = new InventoryClickHandler(main, name);
        shuffle();
        design(rn.nextInt(15 - 0 + 1) + 0);
    }

    /**
     * Design the existing inventory.
     */
    public void design(int color) {
        ItemStack item = new ItemStack(Material.STAINED_GLASS_PANE, 1, (short) color);
        ItemMeta itemMeta = item.getItemMeta();
        itemMeta.setDisplayName(ChatColor.DARK_GRAY + "");
        item.setItemMeta(itemMeta);

        ItemStack torch = new ItemStack(Material.REDSTONE_TORCH_ON);
        ItemMeta torchItemMeta = torch.getItemMeta();
        torchItemMeta.setDisplayName(ChatColor.DARK_GRAY + "");
        torch.setItemMeta(torchItemMeta);

        for (int index = 0; index < inv.getSize(); index++) {
            inv.setItem(index, item);
        }

        inv.setItem(4, torch);
        inv.setItem(22, torch);
    }

    /**
     * Shuffles the existing inventory.
     * <p>
     * Note that this does not shuffle the order of the items. This only shuffles the starting index.
     */
    public void shuffle() {
        int startingIndex = ThreadLocalRandom.current().nextInt(
                this.contents.size());
        for (int index = 0; index < startingIndex; index++) {
            for (int itemstacks = 0; itemstacks < inv.getSize(); itemstacks++) {
                inv.setItem(itemstacks, contents.get((itemstacks + itemIndex) % contents.size()));
            }
            itemIndex++;
        }
    }


    /**
     * This spins the inventory. Call this to play the animation and give a
     * random item.
     *
     * @param seconds  the amount of second you want this to spin.
     * @param reciever player who should recieve the item.
     */
    public void spin(final double seconds, final Player reciever) {
        reciever.openInventory(this.inv);
        new BukkitRunnable() {
            double delay = 0;
            int ticks = 0;

            public void run() {
                ticks++;
                delay += 1 / (20 * seconds);
                if (ticks > delay * 10) {
                    ticks = 0;
                    design(rn.nextInt(15 - 0 + 1) + 0);
                    reciever.playSound(reciever.getLocation(), Sound.NOTE_PIANO, 1f, 8);

                    for (int itemstacks = 9; itemstacks < 18; itemstacks++)
                        inv.setItem(itemstacks,
                                contents.get((itemstacks + itemIndex) % contents.size()));

                    itemIndex++;

                    if (delay >= 1) {
                        cancel();
                        ItemStack test = contents.get((4 + itemIndex - 1) % contents.size());
                        reciever.playSound(reciever.getLocation(), Sound.LEVEL_UP, 1f, 1f);
                        RewardManager.giveReward(test.getItemMeta().getDisplayName(), reciever);
                    }
                }
            }
        }.runTaskTimer(plugin, 0, 1);
    }
}

class InventoryClickHandler implements Listener {
    private String name = null;

    public InventoryClickHandler(Plugin plugin, String inventoryName) {
        Bukkit.getPluginManager().registerEvents(this, plugin);
        this.name = inventoryName;
    }

    @EventHandler
    public void onClick(InventoryClickEvent e) {
        if (e.getInventory().getTitle().equals(name)) {
            e.setCancelled(true);
        }
    }

}



