package ch.verttigo.craftok.dailyrewards.Utils;

import ch.verttigo.craftok.dailyrewards.DL;
import ch.verttigo.craftok.dailyrewards.Managers.RewardManager;
import ch.verttigo.craftok.dailyrewards.Managers.SettingsManager;
import de.dytanic.cloudnet.driver.CloudNetDriver;
import de.dytanic.cloudnet.ext.bridge.player.ICloudPlayer;
import de.dytanic.cloudnet.ext.bridge.player.IPlayerManager;
import net.wesjd.anvilgui.AnvilGUI;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import java.util.ArrayList;

public class Utils {

    public static final IPlayerManager playerManager = CloudNetDriver.getInstance().getServicesRegistry().getFirstService(IPlayerManager.class);

    public static ICloudPlayer getPlayer(String s) {
        for (ICloudPlayer p : playerManager.getOnlinePlayers()) {
            if (s.equalsIgnoreCase(p.getName())) return p;
        }
        return null;
    }

    public static void noPermission(Player player) {
        String msg = SettingsManager.getMsg().getString("no-permission");
        msg = msg.replace("%player", player.getName());
        player.sendMessage(msg);

    }

    public static void addRewardGUI(Player p) {
        ArrayList<String> params = new ArrayList<>();
        new AnvilGUI.Builder()
                .onComplete((player, text) -> {
                    params.add(text);
                    return AnvilGUI.Response.close();
                })
                .onClose(player -> {
                    askForMat(p, params);
                })
                .preventClose()
                .text("Nom du reward")
                .itemLeft(new ItemStack(Material.IRON_SWORD))
                .plugin(DL.plugin)
                .open(p);
    }

    public static void askForMat(Player p, ArrayList<String> params) {
        new AnvilGUI.Builder()
                .onComplete((player, text) -> {
                    if (Material.getMaterial(text) == null) return AnvilGUI.Response.text("Incorrect.");
                    params.add(text);
                    return AnvilGUI.Response.close();
                })
                .onClose(player -> {
                    askForCommand(p, params);
                })
                .preventClose()
                .text("Material du reward")
                .itemLeft(new ItemStack(Material.IRON_SWORD))
                .plugin(DL.plugin)
                .open(p);
    }

    public static void askForCommand(Player p, ArrayList<String> params) {
        new AnvilGUI.Builder()
                .onComplete((player, text) -> {
                    params.add(text);
                    return AnvilGUI.Response.close();
                })
                .onClose(player -> {
                    askForPercentage(p, params);
                })
                .preventClose()
                .text("Commande à éxecuter")
                .itemLeft(new ItemStack(Material.IRON_SWORD))
                .plugin(DL.plugin)
                .open(p);
    }

    public static void askForPercentage(Player p, ArrayList<String> params) {
        new AnvilGUI.Builder()
                .onComplete((player, text) -> {
                    try {
                        Integer.parseInt(text);
                        params.add(text);
                    } catch (NumberFormatException e) {
                        return AnvilGUI.Response.text("Incorrect.");
                    }
                    return AnvilGUI.Response.close();
                })
                .onClose(player -> {
                    askForDesc(p, params);
                })
                .preventClose()
                .text("Percentage à éxecuter")
                .itemLeft(new ItemStack(Material.IRON_SWORD))
                .plugin(DL.plugin)
                .open(p);
    }

    public static void askForDesc(Player p, ArrayList<String> params) {
        new AnvilGUI.Builder()
                .onComplete((player, text) -> {
                    params.add(text);
                    return AnvilGUI.Response.close();
                })
                .onClose(player -> {
                    RewardManager.addReward(params.get(0), Material.getMaterial(params.get(1)), params.get(2), Integer.parseInt(params.get(3)), params.get(4));
                })
                .preventClose()
                .text("Description du reward")
                .itemLeft(new ItemStack(Material.IRON_SWORD))
                .plugin(DL.plugin)
                .open(p);
    }


}
